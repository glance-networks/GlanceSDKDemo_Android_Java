# One-click Connect
## _Visitor Presence and Signaling API_

## Introduction
Glance Presence and Signalling allows an identified Android application user to notify agent CRM or support systems that the user is actively using the app. The agent side may then signal the app to start screensharing. The agent can initiate and join the screenshare automatically or with one-click.

This document is a Quick-start guide for integrating One-click Connect Screensharing into an Android application.  It assumes you have already integrated the Glance Android SDK as described in the [README](README.md) file.

This functionality is available starting in version 4.5.0 of the SDK. For full documentation on the Presence Visitor API see ****.

**Note:** The version of the document on Gitlab contains the latest version of this document. Your local readme file contains documentation specific to the version you cloned. If you read about features not available in your local version, update your SDK to the latest version on Gitlab.

## Integration
Integration uses the `net.glance.android.Visitor` and `net.glance.android.PresenceVisitor` classes.

### Visitor ID

The Visitor Id is a string that uniquely identifies a user connected to the One-Click service.  It is typically an account or user id associated with an authenticated user.  The same visitor id would be used when the same visitor is logged into the app on another device, or in a web application instrumented for Glance One-click Cobrowse.

Only one user can be signalable by the One-Click service at a time with a given Visitor Id. If two application instances connect using the same Visitor Id, the most recently active application has "focus" and will receive any signals sent by the Agent.

#### Specifying the Visitor ID

In your call to `Visitor.init` add an additional parameter for `visitorId`.

```java
    // Configure Glance Visitor SDK
    Visitor.init(this, GLANCE_GROUP_ID, "", "", "", "", "123456");
```

And within the dependencies block of the build.gradle file, add the following:

```
implementation "android.arch.lifecycle:extensions:1.1.0"
annotationProcessor "android.arch.lifecycle:compiler:1.1.0"
```

An application may call `GlanceVisitor init:` multiple times to specify a Visitor Id or not,
depending on whether the user is logged into the application.  Typically an application will call
`init` once on application startup without a Visitor Id, and again upon user login.

#### Starting Sessions once a Visitor Id has been specified
If a Visitor Id has been specified, it will be the default key for any screensharing session. Calling `GlanceVisitor startSession:` with no key specified will start a session using the Visitor Id as the key.  This allows an Agent who knows the identity of the visitor to join the session without the Visitor having to communicate a session key.

If using the Default UI, the session key will not be displayed to the user in the case where a Visitor Id was used as the session key, since it is not necessary for the user to see or communicate the key.

With a Visitor Id specified, it is still possible to start a session with a random key by passing @"GLANCE_KEYTYPE_RANDOM" as the session key to the call to `startSession`.  A best practice is to provide some way in the user interface to start a session with a random key as a fallback, in case the Agent is unable to locate the Visitor's record in the CRM.

### Events
Presence events are delivered to your `onGlanceVisitorEvent` delegate method.

You may want to handle these:

```java
    if (event.getCode() == EventCode.EventPresenceConnected ) {
        // Presence connected
    } else if (event.getCode() == EventCode.EventPresenceConnectFail ) {
        // Presence connect failed. Log error message via event.getMessageString()
    } else if (event.getCode() == EventCode.EventPresenceConnectFail ) {
        // Presence send failed. Log error message via event.getMessageString()
    }
```

### Starting and Stopping
To start Presence after receiving EventVisitorInitialized call `connect`

```java
    PresenceVisitor.connect();
```

To stop (normally on application exit) call `disconnect`

```java
    PresenceVisitor.disconnect();
```

### Navigation Information
When Presence is connected, information can be sent to the agent side whenever the user navigates through the app.

```java
    Map<String, String> url = new HashMap<>();
    url.put("url", "root view");
    PresenceVisitor.presence(url);
```

When your app becomes active or enters the background state the SDK will also notify the agent side.

### Local Notifications

Presence has the ability to alert the user when the app enters the background state. If the agent reaches out to your app while it is backgrounded, a local notification can be triggered by the SDK. This will require the notification permission from your app. You can implement this yourself by asking the user, or you can use the following:

```java
    PresenceVisitor.connect(true);
```

which, on connecting to Presence, will prompt the user to allow push notifications (if they haven't already accepted). Once accepted, notifications will be sent to the user when the app is in the background and an agent attempts to make contact with the user.

### Customizing the UI
The SDK provides a default user interface to request confirmation and show Terms and Conditions.  The URL to a Terms web page is configured in the agent-side CRM system.

To provide your own UI, after calling `connect` call:

```java
    PresenceVisitor.setDefaultUI(false);
```

Then handle the `EventPresenceShowTerms` event.
In your event handing you should:
1. Display your terms and conditions and options to accept (start screenshare) or decline.
2. Notify the agent terms have been displayed
3. Notify the agent of accept or decline
4. On accepting, call `Visitor.startSession()`

An example:

```java
    else if (event.getCode() == EventCode.EventPresenceShowTerms ) {
        myCustomUI(event.GetValue("termsurl"))
    }
```

```java
    private void myCustomUI(final String termUrl) {
        final Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            // setup the alert builder
            AlertDialog.Builder builder = new AlertDialog.Builder(YourActivity.this);
            builder.setTitle("Allow the agent to view this app?");

            // add the buttons
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Map<String, String> terms = new HashMap<>();
                    terms.put("status", "accepted");
                    PresenceVisitor.signalAgent("terms", terms);
                    Visitor.startSession();
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Map<String, String> terms = new HashMap<>();
                    terms.put("status", "declined");
                    PresenceVisitor.signalAgent("terms", terms);
                }
            });

            builder.setNeutralButton("Show Terms", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            // Show your Terms View Activity here
                        }
                    });
                }
            });

            // create and show the alert dialog
            AlertDialog dialog = builder.create();
            dialog.show();

            Map<String, String> terms = new HashMap<>();
            terms.put("status", "displayed");
            PresenceVisitor.signalAgent("terms", terms);
        });
    }
```
