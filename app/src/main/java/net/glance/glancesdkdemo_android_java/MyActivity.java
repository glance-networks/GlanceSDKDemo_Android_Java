package net.glance.glancesdkdemo_android_java;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import net.glance.android.Event;
import net.glance.android.EventCode;
import net.glance.android.EventType;
import net.glance.android.PresenceVisitor;
import net.glance.android.Settings;
import net.glance.android.Visitor;
import net.glance.android.VisitorListener;

import java.net.ProxySelector;
import java.util.HashMap;
import java.util.Map;

// Visitor demo implementation is below. NOTE:
// before reading through this code, make sure you've read through the README

// make sure your class implements VisitorListener
public class MyActivity extends AppCompatActivity implements VisitorListener {
    private static String sessionKey = null;

    private static int GLANCE_GROUP_ID = 15687;
    // Uncomment "123four" to enable Presence code
    // If you are not using Presence you do not need to implement any code within if (VISITOR_ID != nil)
    private static String VISITOR_ID = null; // "123four";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        if (VISITOR_ID != null) {
            // Init with visitor id for Presence
            Visitor.init(this, GLANCE_GROUP_ID, "", "", "", "", VISITOR_ID);
        } else {
            // Initialize the Visitor with the group ID provided by Glance
            Visitor.init(this, GLANCE_GROUP_ID, "", "", "", "", "");
        }

        // Make sure the Visitor is listening for events on this Activity
        Visitor.addListener(this);

        // Mask pin
        Visitor.addMaskedViewId(R.id.textView2);

        // Uncomment these lines to use your own custom UI, including custom agent video viewer
        // Visitor.defaultUI(false); // disable default UI
        // Visitor.setCustomSessionViewId(R.id.glance_agent_viewer);
    }

    @Override
    protected void onResume(){
        super.onResume();

        updateView();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        PresenceVisitor.disconnect();

        Visitor.removeMaskedViewId(R.id.textView2);

        // Make sure to remove the listener before this Activity is destroyed
        Visitor.removeListener(this);
    }

    // this is called on the Glance Event thread
    public void onGlanceVisitorEvent(Event event) {
        if (event.getCode() == EventCode.EventConnectedToSession){
            // Show any UI to indicate the session has started
            // If not using a known session key, you can get the random key here (sessionKey)
            // to display to the user to read to the agent
            sessionKey = event.GetValue("sessionkey");
            updateView();
        }
        else if (event.getCode() == EventCode.EventSessionEnded ){
            // Show any UI to indicate the session has ended
            sessionKey = null;
            updateView();
        }
        else if (event.getType() == EventType.EventWarning ||
                event.getType() == EventType.EventError ||
                event.getType() == EventType.EventAssertFail) {
            // Best practice is to log code and message of all events of these types
            Log.d("EVENT_TYPE", event.getType().toString() + " " + event.getMessageString());
        }

        if (VISITOR_ID != null) {
            if (event.getCode() == EventCode.EventVisitorInitialized) {
                PresenceVisitor.connect();
            } else if (event.getCode() == EventCode.EventPresenceConnected) {
                presenceConnected();
            } else if (event.getCode() == EventCode.EventPresenceConnectFail) {
                Log.d("EVENT_TYPE", "Presence connection failed (will retry): " + event.getMessageString());
            } else if (event.getCode() == EventCode.EventPresenceShowTerms) {
                // You only need to handle this event if you are providing a custom UI for terms and/or confirmation
                Log.d("EVENT_TYPE", "Agent signalled ShowTerms");
            } else if (event.getCode() == EventCode.EventPresenceBlur) {
                // This event notifies the app that the visitor is now using another app or website
            }
        }
    }

    private void updateView() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Button startSessionButton = (Button)findViewById(R.id.startSession);
                Button endSessionButton = (Button)findViewById(R.id.endSession);
                if (sessionKey != null){
                    startSessionButton.setVisibility(View.GONE);
                    endSessionButton.setVisibility(View.VISIBLE);
                    endSessionButton.setEnabled(true);
                    ((TextView)findViewById(R.id.textView2)).setText(sessionKey);
                }else {
                    startSessionButton.setVisibility(View.VISIBLE);
                    startSessionButton.setEnabled(true);
                    endSessionButton.setVisibility(View.GONE);
                    ((TextView)findViewById(R.id.textView2)).setText("Click button to below to start session");
                }
            }
        });
    }

    private void presenceConnected() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((TextView)findViewById(R.id.presenceIndicator)).setText("Presence Connected");
                findViewById(R.id.startSession).setVisibility(View.GONE);
            }
        });
    }

    public void startSession(View v) {
        Button button = findViewById(R.id.startSession);
        button.setEnabled(false);
        // Start session and let Glance generate a session key
        Visitor.startSession();
    }

    public void endSession(View v) {
        Button button = findViewById(R.id.endSession);
        button.setEnabled(false);
        // End Glance session
        Visitor.endSession();
    }

    public void openBrowser(View v) {
        final Context context = this;
        Intent intent = new Intent(context, WebView.class);
        startActivity(intent);

        if (VISITOR_ID != null) {
            Map<String, String> url = new HashMap<>();
            url.put("url", "webview https://www.glance.net");
            PresenceVisitor.presence(url);
        }
    }
}
