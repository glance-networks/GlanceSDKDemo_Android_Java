package net.glance.glancesdkdemo_android_java;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.widget.ImageView;

import net.glance.android.SessionView;

public class RoundedAgentViewer extends SessionView {
    private Path path = new Path();

    public RoundedAgentViewer(Context context) {
        super(context);
        configure();
    }

    public RoundedAgentViewer(Context context, AttributeSet attrs) {
        super(context, attrs);
        configure();
    }

    public RoundedAgentViewer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        configure();
    }

    private void configure(){
        this.setScaleType(ImageView.ScaleType.CENTER_CROP);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        // compute the path
        float halfWidth = w / 2f;
        float halfHeight = h / 2f;
        float centerX = halfWidth;
        float centerY = halfHeight;
        path.reset();
        path.addCircle(centerX, centerY, Math.min(halfWidth, halfHeight), Path.Direction.CW);
        path.close();
    }

    @Override
    protected void dispatchDraw(Canvas canvas) {
        int save = canvas.save();
        canvas.clipPath(path);
        super.dispatchDraw(canvas);
        canvas.restoreToCount(save);
    }
}
