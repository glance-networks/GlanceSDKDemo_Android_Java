# Glance Android SDK Integration

_**This repository contains older SDK versions and sample projects.  For the latest please see [GlanceSDKDemos-Android](https://gitlab.com/glance-networks/glancesdkdemos-android)**_

## Visitor Sessions
###### (c) 2018-2019 Glance Networks, Inc. - All Rights Reserved

**Note**: The readme available on Gitlab contains the latest version of this document. Your local readme file contains documentation specific to the version you cloned. If you read about features not available in your local version, update your SDK to the latest version on Gitlab.

The Glance Android SDK will display the mobile application or device screen to a remote agent. Once the session
starts showing, the SDK will track view changes, activity changes, and device rotation.

This document describes the more common use case of **_Visitor_** sessions.  
A Visitor session is started by a user of your app that is not an authenticated Glance user, though they may be an authenticated user of your app.  The session is joined by a customer support or sales *Agent* that is an authenticated Glance user.

See (other documentation) for authenticating users with the **User** class and starting authenticated **HostSessions** to be joined by guests who are not Glance users.


To integrate with the SDK, an application needs to take the following steps, each outlined in
their own section:
1. Get an agent account and Group ID from Glance
2. Include the AAR
3. Initialize the Visitor class
4. Set an Event handler
5. Start a Visitor session
6. Confirm it's working
7. End the session
8. (Optional) Permissions to capture entire screen (all applications)
version is Lollipop or later, the session will request the appropriate
permissions... (_Do we have this?_)
9. (Optional) Masking Views
the remote agent.
10. Default UI
11. (Optional) Agent Video Viewer Session
12. (Optional) Specify a Custom Agent Video Viewer
13. (Optional) Specify Custom Settings

### Get an Agent Account and Group ID from Glance

Contact Glance at mobilesupport@glance.net to set up a Group (organization account) and an individual Agent account.
You will receive:
1. A numeric Group ID
2. An agent username, for example agnes.example.glance.net (Glance usernames are in the form of DNS names)
3. An agent password

In production usage, agents will typically authenticate with a single-sign-on, often via their CRM implementation (e.g. Salesforce).


### Include the AAR
To include the AAR in the app, put it in the app/libs folder.
Then modify the application’s build.gradle file to include the following just below "apply plugin:
'com.android.application'":

```
repositories {
    flatDir {
        dirs 'libs'
    }
}
```

Ensure multiDexEnabled is set to true within the android > defaultConfig section:

```
android {
    defaultConfig {
        minSdkVersion 19

        ...

        // Enabling multidex support.
        multiDexEnabled true
    }
    ...
}
```

And within the dependencies block of the build.gradle file, add the following:

```
implementation(name: 'glanceSDK', ext: 'aar')
```

Then, sync gradle by going to Tools -> Android -> Sync Project with Gradle Files or clicking the
icon. Until you do this, Android Studio won’t be able to autocomplete or import the
appropriate classes.

### Optimized Builds

The Glance AAR includes a binary build for each supported native architecture.  The Glance library uses the Android NDK and requires native support for each platform.  

The Glance AAR file includes the binaries for each architecture and is larger as a result.  To create an optimized and much smaller APK that only includes the library binary for a single architecture you need to take advantage of APK splits.

[Follow the APK splits documentation to create optimized APKs](https://developer.android.com/studio/build/configure-apk-splits).

### Intitialize the Visitor Class

You must call Visitor.init once to initialize the visitor class for your group.  Pass a reference to your main activity and your Group ID as the second parameter.

You may also pass optional name, email and phone number strings to identify your user.  This information will be stored in Glance session records and available on reports and via our APIs.
You can pass empty strings or any other values useful to you.  The limits are, name: 62 characters, email: 26 characters, phone: 30 characters.

The third argument to Visitor.init is reserved for future use, you should pass an empty string.

```java
Visitor.init(activity, 1234, "", "Vera Visitor", "vera@example.com", "444-555-1234");
```

An optional visitorid: may also be specified in the call to GlanceVisitor init:.  If a visitorid is specified, it will be used as the session key when startSession is called without a session key parameter.

### Set an Event Handler

Put something like this in your Activity:

```java
public class MyActivity implements VisitorListener {

    @Override
    protected void onCreate(@Nullable final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Visitor.init(this, 1234, "", "Vera Visitor", "vera@example.com", "444-555-1234");
        Visitor.addListener(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        Visitor.removeListener(this);
    }

    // this is called on the Glance Event thread
    public void onGlanceVisitorEvent(Event event) {
        if (event.getCode() == EventCode.EventConnectedToSession){
            // Show any UI to indicate the session has started
            // If not using a known session key, you can get the random key here
            // to display to the user to read to the agent
            final String sessionKey = event.GetValue("sessionkey");
        }
        else if (event.getCode() == EventCode.EventSessionEnded ){
            // Show any UI to indicate the session has ended
        }
        else if (event.getType() == EventType.EventWarning ||
                 event.getType() == EventType.EventError ||
                 event.getType() == EventType.EventAssertFail) {
            // Best practice is to log code and message of all events of these types
        }
    }
}
```

### Start a Visitor Session

You may start a session:
- Using a specific session key passed in by the Application
- Using a unique VisitorId specified in the call to `GlanceVisitor init`
- Using a random key

```java
Visitor.startSession();
```

starts a session using the VisitorId, if any, that was specified in the call to `GlanceVisitor init`.  If no visitor id was specified, starts a session with a random key.
Even if a visitorid has been specified, a session with a random key can still be started by calling:

```java
Visitor.startSession("GLANCE_KEYTYPE_RANDOM")
```

If you have a known value you wish to use for a key, such as a customer id, you can pass it to StartSession.  The key may only contain characters from the Base64URL set, up to 63 characters maximum.

```java
Visitor.startSession("123456789");
```

### Confirm it's Working

To confirm the integration is working, start the session and note the session key.

The agent should then go to:
<https://www.glance.net/agentjoin/AgentJoin.aspx>.  

If not logged in already, the agent will be asked to login:

![](images/agent1.png)

When logged-in a form will be shown to enter the session key.

![](images/agent2.png)

At that point the web viewer will display the screen being shared with the ability to gesture:

![](images/agent3.png)

When the key is known a view can be opened directly with
<https://www.glance.net/agentjoin/AgentView.aspx?username={username}&sesionkey={key}&wait=1>

See (other documentation) for details on integrating the agent viewer with other systems and single-sign-on

### End the Session

The session can be ended from either the agent side or the app side.  To end from the app, call:

```java
Visitor.endSession();
```

In either case the event EventSessionEnded will be sent.

### Permissions to Capture Entire Screen

The SDK will attempt to dynamically request full screen capture permission at runtime on devices running Android Lollipop or later.  If the user accepts, the session will utilize the permission to record the full screen.  If the user rejects the SDK falls back to the solution that is used by pre-Lollipop Android devices to record the screen.

#### Masking and Gestures Outside of the Application
Capturing the full screen on android allows the SDK to record the home screen and other apps.  When the user is outside the context of the app the SDK cannot mask views or display gestures to the user.  This means that keyboards and input fields outside of the app integrating the SDK will be fully visible to the agent which may present security issues.  Consider your app use case carefully before enabling the full screen feature.

#### Masking the Keyboard
Keyboard masking allows the SDK to cover the keyboard when its in use.  This prevents potentially sensitive user information from being communicated to the agent.  This feature only applies when permission to capture the entire screen is enabled and approved by the user.  Otherwise keyboard masking is not necessary because the keyboard is not captured as part of the screenshare.  

As a result of keyboard masking being specific to full screen mode it should be noted that keyboards displayed outside of the app integrating the SDK (other apps on the device and home screen) will not be masked.

To enable keyboard masking:

```
Visitor.maskKeyboard(true);
```



### Masking Views

Specific views can be hidden from the agent by passing it them to the SDK.  There are two ways to mask views: by view id or by passing in the view object.  Masking by ID is the preferred approach that can work reliably within the Android activity lifecycle where activity instances are rapidly created and destroyed for a variety of reasons.

#### By View ID

```java
Visitor.addMaskedViewId(R.id.password_view);
```

```java
Visitor.removeMaskedViewId(R.id.password_view);
```

#### By View Object

```java
View passwordView = findViewById(R.id.password_view);
Visitor.addMaskedView(passwordView);
```

```java
View passwordView = findViewById(R.id.password_view);
Visitor.removeMaskedView(passwordView);
```

#### Default UI

Glance provides a default user interface for screenshare, agent video and voice sessions.  

You can customize the default interface colors, text, and graphics to match your company’s branding, see [Default User Interface](https://help.glance.net/docs/mobile-app-sharing/glance-mobile-sdk/#default_user_interface) for more information.

To build your own custom user interface the default UI needs to be disabled:
```java
Visitor.defaultUI(false);
```

### Agent Viewer Session

When an agent joins with video a small default view will be shown in the corner with the agent video.

To control the agent viewer view specify a custom agent view described in the next section.

### Custom Agent Viewer

A custom agent viewer can show the agent video in a custom view that your application controls.

The custom agent viewer requires a view that is present in all of the activity layouts with a consistent view ID.  

The view type must be a subclass of `net.glance.android.SessionView`.  The custom agent viewer can be customized through the subclassing of `net.glance.android.SessionView` to present the agent in a circle shape or other specific design.

An example custom agent viewer class that presents the agent within a circle:

```java
public class RoundedAgentViewer extends net.glance.android.SessionView {
    public RoundedAgentViewer(Context context) {
        super(context);
        configure();
    }

    public RoundedAgentViewer(Context context, AttributeSet attrs) {
        super(context, attrs);
        configure();
    }

    public RoundedAgentViewer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        configure();
    }

    private void configure(){
        this.setScaleType(ScaleType.CENTER_CROP);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        float radius = (float)canvas.getWidth() / 2.0f;
        Path clipPath = new Path();
        RectF rect = new RectF(0, 0, this.getWidth(), this.getHeight());
        clipPath.addRoundRect(rect, radius, radius, Path.Direction.CW);
        canvas.clipPath(clipPath);
        super.onDraw(canvas);
    }
}
```

Define the view layout and ensure it is present in every activity layout.

Here's the custom agent viewer with id `glance_agent_viewer` layout using the RoundedAgentViewer defined above:

```xml
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout
    xmlns:android="http://schemas.android.com/apk/res/android" android:layout_width="match_parent"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:elevation="2dp"
    android:layout_height="match_parent">

    <RoundedAgentViewer
        android:id="@+id/glance_agent_viewer"
        android:layout_width="120dp"
        android:layout_height="120dp"
        android:layout_marginBottom="60dp"
        android:layout_marginLeft="10dp"
        android:visibility="gone"
        tools:layout_editor_absoluteX="0dp"
        tools:layout_editor_absoluteY="0dp"
        app:layout_constraintLeft_toLeftOf="parent"
        app:layout_constraintBottom_toBottomOf="parent"
        />

</android.support.constraint.ConstraintLayout>
```

The custom agent viewer layout defined above now needs to be included within each layout.  It is recommended to define the layout in its own file above called `agent_viewer.xml` and include it within every activity layout like so:

```xml
<?xml version="1.0" encoding="utf-8"?>
<android.support.constraint.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:app="http://schemas.android.com/apk/res-auto"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context=".MyActivity">

    <include layout="@layout/agent_viewer" />

</android.support.constraint.ConstraintLayout>
```

Pass the view ID `glance_agent_viewer` to enable the customer agent viewer within the Visitor API:

```java
Visitor.setCustomSessionViewId(R.id.glance_agent_viewer)
```

To remove the custom agent viewer behavior:

```java
Visitor.clearCustomSessionViewId()
```

## One-Click Connect

Agent One-Click Connect can be implemented with the [Presence and Signalling API](Presence.md).

## Support

Email [mobilesupport@glance.net](mailto:mobilesupport@glance.net) with any questions.
